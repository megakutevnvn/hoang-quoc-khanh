<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class HelloController extends Controller
{
    public function index(){
    	return view('hello');
        
    }
    public function handleRequest(Request $request){
        // nhan het du lieu co trong form
        echo 'Thông tin vừa nhập nè: ';
        echo '<br>';
        $name = $request->input('name');
        echo 'Tên đầy đủ: '.$name;
        echo '<br>';
        
        //Nhận field đầu vào username
        $username = $request->username;
        echo 'Username: '.$username;
        echo '<br>';
        
        //Retrieve the password input field
        $password = $request->password;
        echo 'Password: '.$password;
     
    }
}