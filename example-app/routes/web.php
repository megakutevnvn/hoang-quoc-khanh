<?php

use App\Http\Controllers\HelloController;
use App\Http\Controllers\HomeController\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserController2;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| 
|
*/

Route::get('/', function () {
    return view('welcome');
});

//dat ten cho route
Route::get('hello', function () {
    return view('hello2');
    
})->name('test2');
//truyen nhieu tham so
Route::get('user/{id}/{name}/{comment}', function($id, $name, $comment) {
    echo "ID của user là : " . $id;
    echo "<br>Tên của user là : " . $name;
    echo "<br> Comment của user: " . $comment;
});
//call controller
Route::get('/contact', [UserController::class, 'index']);
Route::get('a/{name}', [UserController2::class, 'index']);
Route::get('/get-form', [HelloController::class, 'index']);
Route::post('/get-form', [HelloController::class,'handleRequest']);

